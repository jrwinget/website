---
title: "Jeremy R. Winget, Ph.D."
subtitle: "Senior Market Research Analyst"
description: "As a senior analyst, computational social scientist, and educator, my research focuses on understanding and improving decision making at the individual and group level using a combination of methods such as experimentation, agent-based modeling, and surveys. I'm also passionate about using data science to gain insights into complex problems and developing tools to make data analysis more accessible and enjoyable."
images:
  - img/home.jpeg
image_left: false
text_align_left: true
show_social_links: true # specify social accounts in site config
show_action_link: true
action_link: /about
action_label: "About me &rarr;"
action_type: text # text, button
type: home
---

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
