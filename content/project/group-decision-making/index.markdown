---
title: 'Group decision-making'
date: '2019-03-26'
slug: group-decision-making
categories:
  - Research
tags:
author: "R. S. Tindale & J. R. Winget"
publication_types:
  - '6'
publication: 'The Oxford Research Encyclopedia of Psychology'
abstract: |
  Group decisions are ubiquitous in everyday life. Even when decisions are made individually, decision-makers often receive advice or suggestions from others. Thus, decisions are often social in nature and involve multiple group members. This entry summarizes the current state of the field concerning the various ways in which groups make decisions. The literature is conceptualized as falling along two dimensions: how much interaction or information exchange is allowed among the group members and how the final decision is made. On one end, group decisions can be made simply by aggregating member preferences or judgments without any interaction among members and where members have no control or say in the final judgment. One the other end, groups decisions can involve extensive member interaction and information exchanges and the final decision is reached by group consensus. In between these two endpoints, various other strategies are also possible, including prediction markets, Delphi groups, and judge-advisor systems. The entry first describes each type of decision making strategy and then discusses relevant research literature. Each section explores how different group decision strategies influence both the quality of the decisions and the processes through which they are reached. The entry ends with a discussion of what aspects of group decision making seem most important for leading to quality group decisions.
summary: 'A book chapter summarizing the current state of the field concerning group decision making. We use two dimensions to organize and explore group decision strategies: (1) the degree of information exchange among group members, and (2) how the final decision is made.'
featured: no
image:
  caption: '[Photo by Annie Spratt on Unsplash](https://unsplash.com/photos/QckxruozjRg)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
- icon: doi
  icon_pack: ai
  name: Publication
  url: https://doi.org/10.1093/acrefore/9780190236557.013.262
- icon: file-pdf
  icon_pack: fas
  name: Preprint
  url: https://psyarxiv.com/kq2ft
---

## Abstract

Group decisions are ubiquitous in everyday life. Even when decisions are made individually, decision-makers often receive advice or suggestions from others. Thus, decisions are often social in nature and involve multiple group members. This entry summarizes the current state of the field concerning the various ways in which groups make decisions.  The literature is conceptualized as falling along two dimensions: how much interaction or information exchange is allowed among the group members and how the final decision is made. On one end, group decisions can be made simply by aggregating member preferences or judgments without any interaction among members and where members have no control or say in the final judgment. One the other end, groups decisions can involve extensive member interaction and information exchanges and the final decision is reached by group consensus. In between these two endpoints, various other strategies are also possible, including prediction markets, Delphi groups, and judge-advisor systems. The entry first describes each type of decision making strategy and then discusses relevant research literature. Each section explores how different group decision strategies influence both the quality of the decisions and the processes through which they are reached. The entry ends with a discussion of what aspects of group decision making seem most important for leading to quality group decisions.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.1093/acrefore/9780190236557.013.262' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.31234/osf.io/kq2ft' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
