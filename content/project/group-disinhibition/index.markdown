---
title: 'Discharged in D.C.: The role of disinhibition in the behavior of insurrection group members'
date: '2022-03-03'
slug: group-disinhibition
categories:
  - Research
tags:
author: "J. R. Winget & E. S. Park"
publication_types:
  - '6'
publication: 'Group Dynamics: Theory, Research, and Practice'
abstract: 'Objective: We use the motivational systems theory of group involvement to explain aspects of the insurrection that occurred on January 6, 2021. <br> Method: Drawing from existing literature, we explain why group involvement is likely to induce perceptions of “safety in numbers” and “strength in numbers.” We further describe why these states are theorized to intensify group members’ perceived sense of entitlement and their subsequent desires to attain power, status, and resources. <br> Results: The motivational systems theory of group involvement framework explains how group membership and interaction can reduce inhibitions and avoidance motivation, while simultaneously intensifying motivational tendencies associated with an approach orientation. <br> Conclusions: This theory paper explains why people in groups are more likely than individuals to be disinhibited during their pursuit of power and pleasure, leading members of insurrectionist groups to be especially impulsive, reckless, and violent.'
summary: "A theory paper summarizing the role of motivational systems in group members' behavior during the January 6th, 2021 attack on the US Capitol building."
featured: no
image:
  caption: '[Photo by TapTheForwardAssist on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:DC_Capitol_Storming_IMG_7965.jpg)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
- icon: doi
  icon_pack: ai
  name: Publication
  url: https://doi.org/10.1037/gdn0000182
- icon: file-pdf
  icon_pack: fas
  name: Preprint
  url: https://psyarxiv.com/z8kj4/
---

## Abstract

**Objective:** We use the motivational systems theory of group involvement to explain aspects of the insurrection that occurred on January 6, 2021. <br>
**Method:** Drawing from existing literature, we explain why group involvement is likely to induce perceptions of "safety in numbers" and "strength in numbers." We further describe why these states are theorized to intensify group members’ perceived sense of entitlement and their subsequent desires to attain power, status, and resources. <br>
**Results:** The motivational systems theory of group involvement framework explains how group membership and interaction can reduce inhibitions and avoidance motivation, while simultaneously intensifying motivational tendencies associated with an approach orientation. <br>
**Conclusions:** This theory paper explains why people in groups are more likely than individuals to be disinhibited during their pursuit of power and pleasure, leading members of insurrectionist groups to be especially impulsive, reckless, and violent.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.1037/gdn0000182' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.31234/osf.io/z8kj4' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
