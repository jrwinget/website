---
title: Projects
description: "My portfolio includes articles, books, and software I have contributed to, as well as openly available educational resources I have created."
author: "Jeremy R. Winget"
show_post_thumbnail: true
show_author_byline: false
show_post_date: false
# for listing page layout
layout: list-grid # list, list-sidebar, list-grid

# set up common front matter for all individual pages inside project/
cascade:    
  author: "Jeremy R. Winget"
  show_author_byline: true
  show_post_date: true
  show_comments: true # see site config to choose Disqus or Utterances
  # for single-sidebar layout only
  sidebar:
    text_link_label: View all projects
    text_link_url: /project/
    show_sidebar_adunit: true # show ad container
---

** No content below YAML for the project _index. This file provides front matter for the listing page layout and sidebar content. It is also a branch bundle, and all settings under `cascade` provide front matter for all pages inside blog/. You may still override any of these by changing them in a page's front matter.**