---
title: 'Learning while deciding in groups'
date: '2017-06-15'
slug: group-learning
categories:
  - Research
tags:
author: "R. S. Tindale & J. R. Winget"
publication_types:
  - '6'
publication: 'The Oxford Handbook of Group and Organizational Learning'
abstract: 'Groups are used to make many important societal decisions. Similar to individuals, by paying attention to the information available during the decision processes and the consequences of the decisions, groups can learn from their decisions as well. In addition, group members can learn from each other by exchanging information and being exposed to different perspectives. However, groups make decisions in many different ways and the potential and actual learning that takes place will vary as a function of the manner in which groups reach consensus. This chapter reviews the literature on group decision making with a special emphasis on how and when group decision making leads to learning. We argue that learning is possible in virtually any group decision-making environment, but freely interacting groups create the greatest potential for learning. We also discuss when and why groups may not always take advantage of the learning potential.'
summary: 'A book chapter summarizing the group decision making literature, describing when and how group decision making facilitates learning.'
featured: no
image:
  caption: '[Photo by Kimberly Farmer on Unsplash](https://unsplash.com/photos/lUaaKCUANVI)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
- icon: doi
  icon_pack: ai
  name: Publication
  url: https://doi.org/10.1093/oxfordhb/9780190263362.013.42
- icon: file-pdf
  icon_pack: fas
  name: Preprint
  url: https://psyarxiv.com/8ufgh
---

## Abstract

Groups are used to make many important societal decisions. Similar to individuals, by paying attention to the information available during the decision processes and the consequences of the decisions, groups can learn from their decisions as well. In addition, group members can learn from each other by exchanging information and being exposed to different perspectives. However, groups make decisions in many different ways and the potential and actual learning that takes place will vary as a function of the manner in which groups reach consensus. This chapter reviews the literature on group decision making with a special emphasis on how and when group decision making leads to learning. We argue that learning is possible in virtually any group decision-making environment, but freely interacting groups create the greatest potential for learning. We also discuss when and why groups may not always take advantage of the learning potential.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.1093/oxfordhb/9780190263362.013.42' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.31234/osf.io/8ufgh' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
