---
title: PSYC 306 - Research Methods
date: 2021-08-30
authors: Jeremy R. Winget
summary: An undergraduate-level course on the fundamentals of research methods in the social sciences.
external_link: ""
image:
  caption: '[Research Scene Vector.svg](https://commons.wikimedia.org/wiki/File:Research_Scene_Vector.svg##) from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) by [Videoplasty.com](https://videoplasty.com/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)'
  focal_point: Smart

links:
- icon: images
  icon_pack: fas
  name: Materials
  url: https://osf.io/6v2ad/

categories:
- Education
tags:
- statistics
---

* Semesters taught:
  + *Fall 2017, Fall 2018, Summer 2019, Summer 2020, Summer 2021, Fall 2021*

This course is designed as an introduction to research methods (and to some extent statistics) used in psychological research. While we will spend a lot of time talking about true experiments, we also discuss non-experimental methodologies (including observation, correlational research, surveys, archival research) and quasi-experimental (including person-by-treatment, natural experiments, nature and treatment, and patch-up designs). Other topics include an introduction to the scientific method, the philosophy of science, measurement and error, descriptive statistics, statistical inference, scientific writing, and ethical considerations in research.
