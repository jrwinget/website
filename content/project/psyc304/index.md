---
title: PSYC 304 - Statistics
date: 2021-01-18
authors: [jeremy]
summary: An undergraduate-level course on the fundamentals of statistics and hypothesis testing.
external_link: ""
image:
  caption: '[Photo on Pixabay](https://pixabay.com/vectors/graphic-progress-diagram-1714230/)'
  focal_point: Smart

links:
- icon: images
  icon_pack: fas
  name: Materials
  url: https://osf.io/dc5mp/

categories:
- Education
tags:
- statistics
---

* Semesters taught:
  + *Summer 2017, Summer 2018, Spring 2020, Spring 2021*

This course emphasizes more of a conceptual understanding of statistics rather than a purely mathematical understanding of statistics; however, notions and formulas for all the procedures discussed will be covered as well. The goal is to gain an understanding of the concepts underpinning statistics, research methods, and data analysis to develop skills to interpret and critique empirical evidence. There will be times when students will need to perform some calculations by hand. These calculations are presented in the service of a deeper understanding of the concept. Ultimately, success in this course will depend on an understanding of when and why specific statistical tools are used. This course will primarily be lecture-driven, though time is also reserved for activities and applications. Examples and sample problems will be heavily emphasized. Additionally, some course time will be spent learning how to use computer programs to conduct statistical analyses.
