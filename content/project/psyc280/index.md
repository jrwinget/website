---
title: PSYC 280 - Judgment and Decision Making
date: 2021-08-30
authors: [jeremy]
summary: An undergraduate-level course on understanding issues related to globalization through psychological theory and research.
external_link: ""
image:
  caption: '[Photo by Vladislav Babienko on Unsplash](https://unsplash.com/photos/KTpSVEcU0XU)'
  focal_point: Smart

links:
- icon: images
  icon_pack: fas
  name: Materials
  url: https://osf.io/dse74/

categories:
- Education
tags:
- psychology
- judgment
- decision making
---

* Semesters taught:
  + *Fall 2019, Fall 2020, Spring 2021, Fall 2021*

This course presents a state-of-the-art discussion of research on judgment and decision-making. Decisions large and small are part of everyday life. Where should I go for lunch? Should I go to the gym today? Will this course make me happy? Is this a cold or the flu? Should I go to the doctor? Is this job for me? Even when we have the best information, we often don't make the right decisions. The quality of our decisions depends upon a variety of factors, many of which can be found in philosophy, psychology, and economic research. Philosophy contributes its canon of literature on inductive and deductive reasoning, and its focus on prescriptive questions about the purpose of good reasoning. Psychology offers experimental evidence of human capability in the area of judgment and decision-making, and the outlines the processes that give rise to good decisions. Economics, as the science of policy, describes the structural conditions that promote good decision-making, and tracks the utilities, costs, and benefits both to individuals and societies of those decisions.

The course examines the impact of psychological biases on personal decision and public policies. It also covers issues such as psychological models of deliberative versus reflective processes in decision-making, and research on happiness and well-being. A goal of this course is to improve everyday reasoning and gain an understanding of the sources of our errors in reasoning.
