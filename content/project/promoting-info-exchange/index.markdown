---
title: 'Promoting information exchange: Open-minded group cognition as a function of motivation and task type'
date: '2021-07-27'
slug: promoting-info-exchange
categories:
  - Research
tags:
  - information exchange
  - decision making
  - problem solving
  - open-minded cognition
  - group dynamics
  - computational modeling
author: "J. R. Winget"
publication_types:
  - '2'
publication: 'Ph.D. Dissertation'
abstract: |
  Open-minded cognition is a cognitive processing style that influences the manner in which individuals select and process information. An open-minded cognitive style is marked by a willingness to consider a variety of intellectual perspectives, values, attitudes, opinions, or beliefs, even those that contradict the individual’s prior opinion. However, people also process information and make decisions within groups, and their individual cognitive styles can influence how the overall group processes and shares information. Therefore, the present paper integrates the open-minded cognition and group decision making literatures, proposes and agent-based model of open-minded group cognition, and empirically tests the antecedents and consequences of open-minded group cognition. Empirical tests were generally supportive of the model; however, some possible boundary conditions were identified. Implications for theory development, practical applications, and directions for future research are discussed.
summary: 'My Ph.D. dissertation in which I propose a computational model of open-minded group cognition and test the model with an empirical experiment.'
featured: no
image:
  caption: '[Photo from Free SVG](https://freesvg.org/open-mind)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
#- icon: doi
#  icon_pack: ai
#  name: Publication
#  url: 
- icon: project-diagram
  icon_pack: fas
  name: Project
  url: https://osf.io/sehmv/
- icon: film
  icon_pack: fas
  name: Video
  url: https://www.youtube.com/watch?v=-vsXBZQQt6c
---

## Abstract

Open-minded cognition is a cognitive processing style that influences the manner in which individuals select and process information. An open-minded cognitive style is marked by a willingness to consider a variety of intellectual perspectives, values, attitudes, opinions, or beliefs, even those that contradict the individual’s prior opinion. However, people also process information and make decisions within groups, and their individual cognitive styles can influence how the overall group processes and shares information. Therefore, the present paper integrates the open-minded cognition and group decision making literatures, proposes and agent-based model of open-minded group cognition, and empirically tests the antecedents and consequences of open-minded group cognition. Empirical tests were generally supportive of the model; however, some possible boundary conditions were identified. Implications for theory development, practical applications, and directions for future research are discussed.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
