---
title: PSYC 360 - Understanding Prejudice
date: 2021-08-30
authors: [jeremy]
summary: An undergraduate-level course that focuses on psychological research and theories that increase our understanding of stereotypes, prejudice, and intergroup relations.
external_link: ""
image:
  caption: '[Photo by Nick Youngson on Alpha Stock Images](https://www.picserver.org/photo/28503/Discrimination.html)'
  focal_point: Smart

links:
- icon: images
  icon_pack: fas
  name: Materials
  url: https://osf.io/8efkx/

categories:
- Education
tags:
- psychology
- prejudice
- discrimination
- intergroup relations
---

* Semesters taught:
  + *Fall 2020, Fall 2021*

This course focuses on psychological research and theories that increase our understanding of stereotypes, prejudice, and intergroup relations. This material considers how the person, the situation, and society shape our thoughts, feelings, and behaviors toward people from various social groups. Most psychological research on these topics focuses on race; however, we also discuss research related to gender, sexual orientation, size, and social class.

Specific topics include the strengths and weaknesses of explanations for the origins of stereotypes and prejudice; prejudice against people in different social groups (e.g., sexual orientation, gender, size, race); variations in racist and sexist beliefs; the effect of stereotypes on how we perceive others and interact socially; how targets of prejudice perceive prejudice, are affected by prejudice, and cope with prejudice; and the psychological processes that may change stereotypes and reduce prejudice.
