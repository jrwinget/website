---
title: 'Deceptive communication in group contexts'
date: '2019-04-30'
slug: group-deception
categories:
  - Research
tags:
author: "J. R. Winget & R. S. Tindale"
publication_types:
  - '6'
publication: 'The Palgrave Handbook of Deceptive Communication'
abstract: |
  Unethical behavior (e.g., deception) is often viewed as an individual-level phenomenon. However, recent research demonstrates the role of group membership on individuals' choices to behave in an ethical or unethical manner (Mannix, Neal, Tenbrunsel, 2006). Compared to individuals, groups tend to be more competitive and self-interested, which sometimes leads groups to be less ethical. Some of this research shows increased tendencies for deception by groups, but not under all circumstances. This chapter outlines the current state of knowledge on whether and when groups will be more likely than individuals to use deception. The chapter focuses on three areas of research. The first involves comparing individuals and groups in mixed motive situations. There is a discontinuity between individual versus group responses to games (e.g., prisoner's dilemma): individuals tend to cooperate while groups tend to compete (Wildschut, Pinter, Veva, Insko, Schopler, 2003). In terms of deception, this is interesting as both individuals and groups initially agree to cooperate. We discuss explanations for the effect and their relation to why groups deceive. Second, we focus on general differences between individuals and groups in deception use. Research shows deception can be beneficial when negotiating, and groups tend to use deception to their benefit more than individuals (Cohen, Gunia, Kim-Jun, Murnighan, 2009; Sutter, 2009). Furthermore, under certain circumstances, groups strategically use honesty to maximize their outcomes. However, other research shows lying is more pronounced under team incentives than individual piece-rates (Conrads, Irlenbusch, Rilke, Walkowitz, 2013). We discuss explanations for these effects and situations where groups would be more versus less likely to use deception. Finally, based on concepts of social identity theory and ingroup bias (Hogg, Abrams, 1988), and work on group decision making (De Dreu, Nijstad, Van Knippenberg, 2008), we provide a framework for understanding when and why groups use deception.
summary: 'A book chapter discussing why groups tend to behave more unethically compared to individuals in the same situations. We also describe when groups would be more (vs. less) likley to use deception.'
featured: no
image:
  caption: '[Photo by R. Stevens](https://crestresearch.ac.uk/comment/culture-deception-detection/)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
- icon: doi
  icon_pack: ai
  name: Publication
  url: https://doi.org/10.1007/978-3-319-96334-1_32
- icon: file-pdf
  icon_pack: fas
  name: Preprint
  url: https://psyarxiv.com/kaqgt
---

## Abstract

Unethical behavior (e.g., deception) is often viewed as an individual-level phenomenon. However, recent research demonstrates the role of group membership on individuals' choices to behave in an ethical or unethical manner (Mannix, Neal, Tenbrunsel, 2006). Compared to individuals, groups tend to be more competitive and self-interested, which sometimes leads groups to be less ethical. Some of this research shows increased tendencies for deception by groups, but not under all circumstances. This chapter outlines the current state of knowledge on whether and when groups will be more likely than individuals to use deception. The chapter focuses on three areas of research. The first involves comparing individuals and groups in mixed motive situations. There is a discontinuity between individual versus group responses to games (e.g., prisoner's dilemma): individuals tend to cooperate while groups tend to compete (Wildschut, Pinter, Veva, Insko, Schopler, 2003). In terms of deception, this is interesting as both individuals and groups initially agree to cooperate. We discuss explanations for the effect and their relation to why groups deceive. Second, we focus on general differences between individuals and groups in deception use. Research shows deception can be beneficial when negotiating, and groups tend to use deception to their benefit more than individuals (Cohen, Gunia, Kim-Jun, Murnighan, 2009; Sutter, 2009). Furthermore, under certain circumstances, groups strategically use honesty to maximize their outcomes. However, other research shows lying is more pronounced under team incentives than individual piece-rates (Conrads, Irlenbusch, Rilke, Walkowitz, 2013). We discuss explanations for these effects and situations where groups would be more versus less likely to use deception. Finally, based on concepts of social identity theory and ingroup bias (Hogg, Abrams, 1988), and work on group decision making (De Dreu, Nijstad, Van Knippenberg, 2008), we provide a framework for understanding when and why groups use deception.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.1007/978-3-319-96334-1_32' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.31234/osf.io/kaqgt' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
