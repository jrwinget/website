---
title: PSYC 100 - Psychological Perspectives on the Experiences of Globalization
date: 2021-08-30
authors: [jeremy]
summary: An undergraduate-level course on understanding issues related to globalization through psychological theory and research.
external_link: ""
image:
  caption: '[Photo on Pixabay](https://pixabay.com/illustrations/earth-globalisation-network-3866609/)'
  focal_point: Smart

links:
- icon: images
  icon_pack: fas
  name: Materials
  url: https://osf.io/3hsqj/

categories:
- Education
tags:
- psychology
- globalization
---

* Semesters taught:
  + *Fall 2021*

This course approaches globalization from a psychological perspective. It is designed to inform students’ understanding of global issues with modern psychological concepts, research, and theory, and to teach students to see psychological issues at work in the experience of globalization. To this end, it is grounded by a set of videos that provide vivid, first-person accounts of real people whose lives have been dramatically affected by and embody critical issues associated with globalization. This biographical information is then illuminated by material contained in studies and review articles chosen for their application of core psychological concepts, research, and theory to issues of behavior and development connected to the experience of globalization. We will also explore social entrepreneurship as a mechanism for dealing with the various complex issues that globalization presents.
