---
title: 'Stereotypic morality: The effects of group membership on moral foundations'
date: '2020-08-01'
slug: stereotypic-morality
categories:
  - Research
tags:
author: "J. R. Winget & R. S. Tindale"
publication_types:
  - '2'
publication: 'Group Processes & Interpersonal Relationships, 23(5), 710-715'
abstract: |
  Today's modern world affords many benefits, one of which is the ability to have near-instantaneous interactions with groups and cultures other than our own. Though advantageous in many situations, one challenge for these groups is navigating what they perceive to be right and wrong in a cooperative manner despite having different modes of morality. Moral foundations theory holds groups use the same moral foundations to guide their judgments and decision making, but there has been little research on how the perception of these foundations differs within and between groups. Thus, the current study examined how moral foundations operate from a group perspective and potential outgroup moderators of moral foundations. Participants rated the extent to which various groups used moral foundations in one of two conditions. Each condition contained an ingroup and three outgroups that conformed to the quadrants of the stereotype content model. Results showed significant differences in the harm, fairness, and loyalty foundations between ingroups and outgroups. Moreover, the type of outgroup significantly influenced moral foundations scores. These findings demonstrate the importance of considering moral foundations at the group level.
summary: 'An experiment demonstrating the effects of group membership on the perception of moral foundations.'
featured: no
image:
  caption: '[Photo commissioned by Adam Thomas and is released for public use with attribution to the artist, Aprilia Muktirina.](https://commons.wikimedia.org/wiki/File:An_illustration_of_Moral_foundations_Theory_created_by_Aprilia_Muktirina.jpg)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
- icon: doi
  icon_pack: ai
  name: Publication
  url: https://doi.org/10.1177/1368430219866502
- icon: file-pdf
  icon_pack: fas
  name: Preprint
  url: https://psyarxiv.com/9u835/
- icon: project-diagram
  icon_pack: fas
  name: Project
  url: https://osf.io/84t9p/
- icon: columns
  icon_pack: fas
  name: Poster
  url: https://osf.io/uakct/
---

## Abstract

Today's modern world affords many benefits, one of which is the ability to have near-instantaneous interactions with groups and cultures other than our own. Though advantageous in many situations, one challenge for these groups is navigating what they perceive to be right and wrong in a cooperative manner despite having different modes of morality. Moral foundations theory holds groups use the same moral foundations to guide their judgments and decision making, but there has been little research on how the perception of these foundations differs within and between groups. Thus, the current study examined how moral foundations operate from a group perspective and potential outgroup moderators of moral foundations. Participants rated the extent to which various groups used moral foundations in one of two conditions. Each condition contained an ingroup and three outgroups that conformed to the quadrants of the stereotype content model. Results showed significant differences in the harm, fairness, and loyalty foundations between ingroups and outgroups. Moreover, the type of outgroup significantly influenced moral foundations scores. These findings demonstrate the importance of considering moral foundations at the group level.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.1177/1368430219866502' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.31234/osf.io/9u835' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
