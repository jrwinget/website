---
title: 'Distributed cognition in teams is influenced by type of task and nature of member interactions'
date: '2020-09-29'
slug: distrib-team-cog
categories:
  - Research
tags:
author: "R. S. Tindale, J. R. Winget, & V. B. Hinsz"
publication_types:
  - '6'
publication: 'In McNeese, Salas, & Endsley (Eds), *Foundations and Theoretical Perspectives of Distributed Team Cognition* (pp. 91-113)'
abstract: |
  In contemporary organizations, many—if not most—teams work on cognitive or information processing tasks (Hinsz, Tindale, & Vollrath, 1997). The past 50 years of research have taught us much about how information is accessed, created, attended to, and processed as teams attempt to complete various tasks. However, many of the information processing effects that have been observed are task specific, yet little research has focused specifically on tasks and how their information processing requirements differ. In this chapter, we discuss how task differences can impact how teams use and process information and how different information distribution patterns across members might impact performance. In addition, we address how constraints on the amount and type of interactions among the team members influences performance in different task domains. We hope our discussion demonstrates the importance of task differences for understanding team information processing and highlights where greater research focus will be fruitful.
summary: 'A book chapter summarizing the group decision making literature, while highlighting the importance of task type for group performance.'
featured: no
image:
  caption: '[Photo on Pixabay](https://pixabay.com/vectors/group-discussion-people-1962592/)'
  focal_point: 'Smart'
  preview_only: no
projects: []
links:
- icon: doi
  icon_pack: ai
  name: Publication
  url: https://doi.org/10.1201/9780429459795
- icon: file-pdf
  icon_pack: fas
  name: Preprint
  url: https://psyarxiv.com/wfy7a/
---

## Abstract

In contemporary organizations, many—if not most—teams work on cognitive or information processing tasks (Hinsz, Tindale, & Vollrath, 1997). The past 50 years of research have taught us much about how information is accessed, created, attended to, and processed as teams attempt to complete various tasks. However, many of the information processing effects that have been observed are task specific, yet little research has focused specifically on tasks and how their information processing requirements differ. In this chapter, we discuss how task differences can impact how teams use and process information and how different information distribution patterns across members might impact performance. In addition, we address how constraints on the amount and type of interactions among the team members influences performance in different task domains. We hope our discussion demonstrates the importance of task differences for understanding team information processing and highlights where greater research focus will be fruitful.

<table border="0">
 <tr>
  <td><b style="font size=30px">Source document:</b></td>
  <td><b style="font size=30px">Preprint:</b></td>
 </tr>
 <tr> 
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.1201/9780429459795' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
  <td><script type="text/javascript" src="https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js"></script><div class='altmetric-embed' data-badge-type='medium-donut' data-badge-details='right' data-doi='10.31234/osf.io/wfy7a' data-hide-no-mentions="true" data-hide-less-than="0"></div></td>
 </tr>
</table>
