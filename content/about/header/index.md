---
## Configure header of page
text_align_right: false
show_title_as_headline: false
headline: |
  Hello! Welcome.
---

<!-- this is a subheadline -->
I'm a senior research analyst at SKIM, and I use advanced quantitative methods to understand and improve how individuals and groups make judgments and decisions. I am also an author and invited speaker, as well as a former academic and consultant, with expertise in decision making, statistics, and research methods.
