---
title: "About me"
description: |
  About Jeremy R. Winget.
show_header: true
sidebar_left: false
# Keep this! Do not edit.
cascade:
  headless: true
---

** index doesn't contain a body, just front matter above.
See the header / main / sidebar folders to edit the index.md files **
