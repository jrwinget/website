---
## Configure page content in wide column
title: "Lately" # leave blank to exclude
number_featured: 1 # pulling from mainSections in config.toml
use_featured: false # if false, use most recent by date
number_categories: 5 # set to zero to exclude
show_intro: true
intro: |
  Currently, I work on understanding how group dynamics influence open-minded cognition and how this may contribute to things like group polarization. My goal is to use these insights to improve individual and group decision making in both digital and physical contexts.
show_outro: true
outro: |
  <i class="fas fa-ghost pr2"></i>Thank you for visiting!
---

** index doesn't contain a body, just front matter above.
See about/list.html in the layouts folder **
