---
## Configure sidebar content in narrow column
author: "Jeremy R. Winget"
role: "Senior Market Research Analyst"
avatar_shape: rounded # circle, square, rounded
show_social_links: true # specify social accounts in site config
audio_link_label: "How to say my name"
link_list_label: "Interested in" # bookmarks, elsewhere, etc.
link_list:
- name: "Group Dynamics"
  url: 
- name: "Information Processing"
  url: 
- name: "Social Influence"
  url: 
- name: "Computational Modeling"
  url :
- name: "Machine Learning"
  url: 
---

** index doesn't contain a body, just front matter above.
See about/list.html in the layouts folder **
