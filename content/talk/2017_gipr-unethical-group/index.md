---
authors: 
abstract: 
abstract_short: ""
date: 2017-06-27T16:50:00
event: Center for Group Research and Group Processes and Intergroup Relations Anniversary Conference
event_url:
image:
  caption:
  focal_point:
location: Canterbury, United Kingdom
math: true
selected: true
title: Do groups make less ethical decisions than individuals?
  
tags: [group dynamics, decision making, ethics]
---
