---
authors: 
abstract: "Groups attempted to recall a story where the parts of the story were distributed among the group members. How much of the information was shared vs. unshared and the presence/absence of a transactive memory system were varied. Both redundancy and transactive memory influenced group recall."
abstract_short: ""
date: 2018-07-20T15:00:00
event: Thirteenth annual INGRoup conference
event_url: http://ingroup.net/past.html
url_slides: https://osf.io/atd8h/
image:
  caption:
  focal_point:
location: Bethesda, Maryland
math: true
selected: true
title: Group recall as a function of information redundancy and transactive memory
  
tags: [group dynamics, transactive memory, information processing]
---
