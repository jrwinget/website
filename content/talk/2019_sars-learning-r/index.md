---
authors: 
abstract: "In this workshop, I will cover an introduction to R for the psychological researcher with little to no programming experience. The content will mostly consist of what you might find in an introductory statistics class. I'll first discuss some benefits of using R, how to get started in R, and provide an introduction to data manipulation and writing scripts. The workshop will also cover descriptive statistics, data vizualization, analysis of contingency tables, t-tests, ANOVAs and regression. I'll end with a discussion of resources to continue learning R."
abstract_short: ""
date: 2019-11-25T13:40:00
event: Loyola University Chicago Social Area Research Series
event_url: 
url_slides: https://gitlab.com/jrwinget/learning-r/-/blob/master/01_slides/learning-r.pdf
url_code: https://gitlab.com/jrwinget/learning-r
image:
  caption:
  focal_point:
location: Chicago, Illinois
math: true
selected: true
title: Learning R
  
tags: [workshop, R, tidyverse]
---
