---
authors: 
abstract: "Often, in teams where members possess diverse knowledge, integrating the unique expertise of members is critical (e.g., interdisciplinary, transdisciplinary teams). In this talk, I will present a process-oriented account of the integration of knowledge in teams when members possess predominately unique (as opposed to shared) information. Within the context of an agent-based model, I outline how the configuration of personality, within teams, function to promote knowledge sharing, selection, assimilation, and ultimately, the integration of information. I will then present results from virtual experiments to highlight the utility of the model and end with a discussion of future plans to validate the model using empirical data."
abstract_short: ""
date: 2020-06-16T13:40:00
event: Thirty-fifth Annual Society for Industrial and Organizational Psychology conference
event_url: 
url_slides: https://osf.io/k34ct/
image:
  caption:
  focal_point:
location: Virtual confrence
math: true
selected: true
title: Process-oriented model of integration in knowledge-diverse teams
  
tags: [computational modeling, agent based model, cognitive complexity, teams]
---
