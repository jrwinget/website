---
authors: 
abstract: "Often, in teams where members possess diverse knowledge, integrating the unique expertise of members is critical (e.g., interdisciplinary, transdisciplinary teams). In this talk, I will present a process-oriented account of the integration of knowledge in teams when members possess predominately unique (as opposed to shared) information. Within the context of an agent-based model, I outline how the configuration of personality, within teams, function to promote knowledge sharing, selection, assimilation, and ultimately, the integration of information. I will then present results from virtual experiments to highlight the utility of the model and end with a discussion of future plans to validate the model using empirical data."
abstract_short: ""
date: 2019-11-11T13:40:00
event: Loyola University Chicago Social Area Research Series
event_url: 
url_slides: 
image:
  caption:
  focal_point:
location: Chicago, Illinois
math: true
selected: true
title: A computational model of information integration in knowledge-diverse teams
  
tags: [computational modeling, agent based model, cognitive complexity, teams]
---
