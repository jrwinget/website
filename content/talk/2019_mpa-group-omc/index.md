---
authors: 
abstract: "The situational merit standard hypothesis states social norms dictate when open-minded condition (OMC) is appropriate. One context we expect normatively less open-mindedness are those involving group interactions. In one study, we manipulated target group membership and measured OMC. In a second study, we manipulated open-minded norms within a group and measured OMC."
abstract_short: ""
date: 2019-04-11T13:00:00
event: Ninety-first Annual Meeting of the  Midwestern Psychological Association
event_url: http://midwesternpsych.org/2019-meeting/
url_slides: https://osf.io/4v29b/
image:
  caption:
  focal_point:
location: Chicago, Illinois
math: true
selected: true
title: Open-minded group cognition
  
tags: [norms, information processing, open-minded cognition]
---
