---
title: Shared representations
summary: How does being a member of a group alter people's judgment? How might information sharing within groups contribute to unfair decisions?
abstract: ""
date: "2016-04-27T00:00:00Z"
image:
  caption: '[Photo on Pixabay](https://pixabay.com/illustrations/team-work-building-puzzle-pieces-3738261/)'
  focal_point: 

categories:
- group-dynamics
- decision-making
- morality
- ethics
---

Research shows that groups tend to engage in a fair amount of unethical behavior (Mannix, Neal, & Tenbrunsel, 2006). Even in situations where individuals behave cooperatively, groups often choose not to cooperate to protect or enhance the group (Wildschut, Pinter, Vevea, Insko, & Schopler, 2003; Morgan & Tindale, 2002). Because of this, groups tend to use the group’s welfare to guide their decision-making and follow their self-interest even when doing so violates typical norms of ethics (Cohen, Gunia, Kim-Jun, & Murnighan, 2009). This seems to guide group behavior in directions opposite of those typically found for individuals and is difficult to change (Tindale, 2008; Wildschut & Insko, 2006; Wildschut et al., 2003). Such unethical conduct by groups is well documented, but what is needed now is more research on promoting ethical relationships among group members. Much like shared choice preferences (e.g., majorities) and commonly shared information (Stasser & Titus, 1985), task representations that are shared among group members tend to guide the group toward choices that are consistent with the representations (Tindale, et al., 1996). For example, groups told that a problem has a correct answer tend to approach the task in a problem-solving manner and are more likely to solve the problem correctly (Stasser & Stewart, 1992).

Using work on shared task representations, one line of my research examines how changing shared representations can promote honest and ethical behavior. This was the basis for Tindale, Shi, Castaneda, & Winget (2017). Individuals or four-person groups were asked to decide as a member of a pharmaceutical company that has a legally approved drug on the market with potentially deadly side effects. Their company is working on newer drugs with no such side effects, but competitors have already released drugs that work for the ailments in question without having deadly side effects. The scenario gives the decision-maker several options that range from pulling the drug off the market immediately (most ethical) to continuing to strongly market the drug (least ethical). We manipulated shared task representations using company mission statements (honesty, competitive, or none). Data collection is still ongoing, but we predict groups that are given a competitive statement, or no statement, would show the greatest degree of unethical behavior (leaving the drug on the market longer) whereas groups with an honesty statement would show the least amount of unethical behavior.

***

## Current Projects:

+ [](#)

***

## Representative Publications:

+ **Winget, J.R.** & Tindale, R.S. (2020). Stereotypic morality: The effects of group membership on moral foundations. *Group Processes and Interpersonal Relationships, 23*(5), 710-725.

***
