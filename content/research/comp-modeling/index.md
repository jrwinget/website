---
title: Computational modeling
summary: What conditions lead to optimal group decision performance? What are practical interventions for biased information exchange?
abstract: ""
date: "2016-04-27T00:00:00Z"
image:
  caption: '[Photo by Elizabeth Virginia Ramsey from NC State University Libraries](https://www.lib.ncsu.edu/workshops/introduction-agents-based-modelling-talk)'
  focal_point: 

categories:
- computational-modeling
- information-processing
- group-dynamics
---

Computational models of complex behavior (e.g., group discussions) offer a variety of benefits. First, such models permit us to link theoretical propositions and empirical findings in an integrated symbolic representation, which then allows us to explore their combined actions (Stasser, 1988). For example, if we know which option people tend to choose out of three responses and how they combine information to make a judgment, then we have the ingredients for a computational model of task decision. This model would take information about choice alternatives as an input and provide a likely choice as an output. Second, computational models allow us to assess whether our understanding of the components of complex behavior is enough to produce an account of the actual behavior we are interested in. While building the model, we might realize that we have an insufficient understanding of the behavior, which could lead to new insights (Larson, 1997; Stasser, 1988). If we can build a working computational model, we can adjust the parameters to explore their effects on the behavior of interest and perhaps learn something new. Finally, computational modeling offers one way of understanding how processes at one level can lead to changes at a higher level (Stasser, 1988). This last point is especially relevant to group decision-making. It is often difficult to link what individuals do within groups to what groups end up doing. For example, although we may understand an individual's behavior in a given situation, it can be difficult to predict how a group of individuals may behave in the same situation because the same group processes can often lead to different outcomes (e.g., Tindale, Smith, Dykema-Engblade, & Kluwe, 2012).

Based on previous research about the nature of group-level information processing and individual-level open-minded cognition, I developed a theory about the influence of open-minded group cognition on group decision performance. The theory proposes that the combination of two orthogonal motivations (epistemic and social), and the type of task groups work on, impact a group member's individual-level open-minded cognition. These group member cognitive styles, in turn, influence how likely they are to exchange information with other members in the group during a group discussion. I implemented this theory in an agent-based model, which is a class of computational models for simulating the actions of agents (e.g., group members) to assess their effects on the system (e.g., group) as a whole. Results from this theory not only supported preregistered a priori predictions, but it pointed to other potential effects for future research as well.

***

## Current Projects:

+ [](#)

***

## Representative Publications:

+ 

***
