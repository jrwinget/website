---
title: Open-minded cognition
summary: How might information sharing within groups contribute to polarization? How can we make groups and individuals more open to opposing views?
abstract: ""
date: "2016-04-27T00:00:00Z"
image:
  caption: '[Photo by Brett Jordan on Unsplash](https://unsplash.com/photos/uPWvYb07JXs)'
  focal_point: 

categories:
- information-processing
- decision-making
---

Research suggests that political polarization is increasing in the United States (e.g., Druckman, Peterson, & Slothus, 2013). In the United States, political polarization is often reflected in increased conflict and political divide between traditionalist conservatives and more progressive liberals. However, there is evidence group polarization is growing as well (e.g., Del Vicario, et al., 2016). Group polarization is the tendency for groups to make more extreme recommendations after discussion compared to the average group member’s recommendation before discussion (Moscovici & Zavalloni, 1969). These forms of polarization are presumably exacerbated by closed-minded cognitive styles and a failure to consider other perspectives or information. Increased understanding of open-minded cognition at the individual- and group-level may foster the development of social interventions designed to decrease polarization, conflict, and unethical decision-making; as well as increase empathy, tolerance, acceptance, and pro-social behavior.

At the individual level, I have examined these ideas primarily through the lens of the flexible merit standard model (Ottati, Wilson, Price, 2015). The flexible merit standard model presumes when individuals think about an issue, they initially activate and select an appropriate “merit standard”. That is, individuals consider the degree to which a closed- versus open-minded orientation is normatively appropriate or merited. Once this injunctive norm is activated, it determines the degree to which the individual adopts a closed- or open-minded cognitive orientation. A corollary of the situational merit standard hypothesis is that some situations will activate less open-mindedness than others. A likely candidate for such situations is those involving group interactions. Winget, Ottati, and Tindale (2019) tested the effects of ingroup on the situationally activated merit standard. We found that when threatening information was presented by an ingroup member compared to an outgroup member, participants were more open-minded. We found further evidence of the  situational merit standard hypothesis by directly manipulating the social norm within the group: When open-minded norms are present within a group, participants were more open-minded compared to being in a group with a close-minded norm.

My dissertation extends the work on open-minded cognition at the individual level to the group level. Since this moves the cognitive style to the group domain, open-minded group cognition not only involves the unbiased cognitive processing in the minds of the group members, it also involves the unbiased cognitive processing (i.e., exchange of information) within the group (c.f., Hinsz et al., 1997). In other words, open-minded group cognition includes unbiased sharing of information within the group, open-minded cognitive styles among the group members, and this unbiased sharing of information and cognitive processes are continuing to be exchanged and transferred within the group. I tested this within the motivated information processing within groups model (De Dreu, Nijstad, & Knippenberg, 2008). Three-person groups worked on a logic puzzle and with either high or low epistemic motivation and prosocial or proself motivation. I also manipulated whether the task they worked on had a demonstrably correct solution or one where they had to make a judgment call. Data collection is still ongoing, but I predict prosocial high epistemic groups to have the highest levels of open-minded group cognition, which in turn should lead them to exchange information in the least biased fashion and to have the highest performance rates. Conversely, proself low epistemic groups should have the lowest levels of open-minded group cognition, which in turn should lead them to exchange information in the most biased fashion and to have the lowest performance rates. I also plan to conduct exploratory analyses of the group discussions using quantitative text analyses (e.g., sentiment analysis, word correlations, etc.) for any potential patterns in discussion remarks across these conditions.

***

## Current Projects:

+ [](#)

***

## Representative Publications:

+

***
