---
title: Research Areas
description: |
  An overview of my main lines of research.
author: "Jeremy R. Winget"
show_post_thumbnail: true
thumbnail_left: false
show_author_byline: false
show_post_date: false
# for listing page layout
layout: list-grid # list, list-sidebar, list-grid

# set up common front matter for all pages inside blog/
cascade:
  author: "Jeremy R. Winget"
  show_author_byline: true
  show_post_date: true
  show_comments: true # see site config to choose Disqus or Utterances
---

** No content below YAML for the research _index. This file provides front matter for the listing page layout and sidebar content. It is also a branch bundle, and all settings under `cascade` provide front matter for all pages inside research/. You may still override any of these by changing them in a page's front matter.**