---
# Display name
name: Jeremy R. Winget

# Username (this should match the folder name)
title: Jeremy R. Winget
authors:
- jeremy

# Is this the primary user of the site?
superuser: true

bio: I'm a behavioral scientist, instructor, and consultant who loves programming.
education:
  courses:
  - course: PhD in Applied Social Psychology
    institution: Loyola University Chicago
    year: 2021
  - course: MA in Applied Social Psychology
    institution: Loyola University Chicago
    year: 2016
  - course: BSc in Psychology/Criminal Justice
    institution: Grand Valley State University
    year: 2014
email: ""
interests:
- Group Dynamics
- Information Processing
- Social Influence
- Computational Modeling
- Machine Learning

organizations:
- name: SKIM
  url: https://skimgroup.com/
  
role: Senior Market Research Analyst

social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/_jrwinget
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/jrwinget
- icon: osf
  icon_pack: ai
  link: https://osf.io/x3gve/
- icon: impactstory
  icon_pack: ai
  link: https://profiles.impactstory.org/u/0000-0002-3783-4354
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-3783-4354
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=UbgwtEwAAAAJ
---

I am a senior analyst, computational social scientist, and educator. My work focuses on understanding and improving individual and group decision-making with mixed methods (e.g., experimentation, agent-based models, surveys, etc.). I am also passionate about using data science to developing insights into complex and challenging problems. I hope to one day build tools to make data analysis easier, open, and fun.
